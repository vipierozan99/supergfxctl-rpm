Name:           supergfxctl
Version:        5.0.1
Release:        1%{?dist}
Summary:        supergfxctl package

License:        GPL
Source0:        https://gitlab.com/asus-linux/supergfxctl/-/archive/%{version}/supergfxctl-%{version}.tar.gz

BuildRequires:  cargo, systemd-devel

%description
RPM build of supergfxctl

%global debug_package %{nil}

%prep
%setup -q

%build
make %{?_smp_mflags} libdir=${_libdir}

%install
%make_install

%files
%license LICENSE
%{_bindir}/supergfxctl
%{_bindir}/supergfxd
%{_libdir}/systemd/system/supergfxd.service
%{_libdir}/systemd/system-preset/supergfxd.preset
%{_datadir}/dbus-1/system.d/org.supergfxctl.Daemon.conf
%{_datadir}/X11/xorg.conf.d/90-nvidia-screen-G05.conf
%{_libdir}/udev/rules.d/90-supergfxd-nvidia-pm.rules





%doc

%changelog
* Fri Dec 16 2022 Victor Pierozan
- 
